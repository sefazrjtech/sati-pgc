# SATIPGC

_Missão_

**Índice**   
1. [Tarefas](#id1)
2. [Nomenclatura GIT](#id1)
3. [Comandos Angular](#id3)



## Tarefas<a name="id1"></a>

- v.0.0.1-beta - 17.08.2018: Make components: topo-governo, [header: menu],footer;
- v.0.0.2-beta - 20.08.2018: Make components: news - componentes padrão para gerar a apresentação do REST. a prícipio somente 1 componente;
- v.0.0.3-beta - 21.08.2018: Organize menu: Organizando o menu com os itens atuais;
- v.0.0.4-beta - 22.08.2018: Melhoria no menu e criação de página interna de notícia;
- v.0.0.5-beta - 24.08.2018: Ajuste no index.js para os componentes;
- v.0.0.6-beta - 03.09.2018: Comentei o script para fixar a barra de menufake api;
- v.0.0.7-beta - 04.09.2018: Página de notícias com dados sendo puxados do json-server-api;



## Nomenclatura GIT<a name="id2"></a>

fonte: [stackoverflow: Branch naming best practices](https://stackoverflow.com/questions/273695/git-branch-naming-best-practices)

#### Sugestão
- __wip__  Trabalho em processo; Alguma coisa que vc não quer acabar agora
- __feat__ Um requisito que estou adicionando ou expandindo
- __bug__  BugFix ou experimento
- __junk__ Throwaway branch created to experiment


#### Como nomear as Branches
```sh
new/frabnotz
new/foo
new/bar
test/foo
test/frabnotz
ver/foo
```

#### Benefícios, buscar branches rapidamente
```sh
$ git branch --list "test/*"
test/foo
test/frabnotz

$ git branch --list "*/foo"
new/foo
test/foo
ver/foo

$ gitk --branches="*/foo"
```
---

## Comandos Angular<a name="id3"></a>


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Servidor de DEV

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Buildar Projeto

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

