import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TopoGovernoComponent } from './components/topo-governo/topo-governo.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderModule } from './components/header/header.module';
import { routing } from './app-routing.module';
import { NewsComponent } from './news/news.component';
import { OqueComponent } from './components/pages/oque/oque.component';
import { OquemudaComponent } from './components/pages/oquemuda/oquemuda.component';
import { CapacitacaoComponent } from './components/pages/capacitacao/capacitacao.component';
import { ResponsabilidadesComponent } from './components/pages/responsabilidades/responsabilidades.component';
import { SgpdComponent } from './components/pages/sgpd/sgpd.component';
import { LegislacaoComponent } from './components/pages/legislacao/legislacao.component';
import { ProjetoComponent } from './components/pages/projeto/projeto.component';
import { MetodgestaoComponent } from './components/pages/metodgestao/metodgestao.component';
import { BaseconhecimentoComponent } from './components/pages/baseconhecimento/baseconhecimento.component';

import { NewsService } from './news.service';

@NgModule({
  declarations: [
    AppComponent,
    TopoGovernoComponent,
    FooterComponent,
    NewsComponent,
    OqueComponent,
    OquemudaComponent,
    CapacitacaoComponent,
    ResponsabilidadesComponent,
    SgpdComponent,
    LegislacaoComponent,
    ProjetoComponent,
    MetodgestaoComponent,
    BaseconhecimentoComponent
  ],
  imports: [BrowserModule, HeaderModule, routing, HttpClientModule],
  providers: [NewsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
