import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";


import { NewsComponent }              from   "./news/news.component";
import { OqueComponent }              from   "./components/pages/oque/oque.component";
import { OquemudaComponent }          from   "./components/pages/oquemuda/oquemuda.component";
import { ProjetoComponent }           from   "./components/pages/projeto/projeto.component";
import { ResponsabilidadesComponent } from   "./components/pages/responsabilidades/responsabilidades.component";
import { SgpdComponent }              from   "./components/pages/sgpd/sgpd.component";
import { BaseconhecimentoComponent }  from   "./components/pages/baseconhecimento/baseconhecimento.component";
import { CapacitacaoComponent}        from   "./components/pages/capacitacao/capacitacao.component";
import { LegislacaoComponent }        from   "./components/pages/legislacao/legislacao.component";
import { MetodgestaoComponent }       from   "./components/pages/metodgestao/metodgestao.component";

const routes: Routes = [
  { path: "", component: NewsComponent },
  { path: "noticias", component: NewsComponent },
  { path: "oquee", component: OqueComponent },
  { path: "oquemuda", component: OquemudaComponent },
  { path: "projeto", component: ProjetoComponent },
  { path: "responsabilidades", component: ResponsabilidadesComponent },
  { path: "sgbd", component: SgpdComponent },
  { path: "baseconhecimento", component: BaseconhecimentoComponent },
  { path: "capacitacao", component: CapacitacaoComponent },
  { path: "legislacao", component: LegislacaoComponent },
  { path: "metodgestao", component: MetodgestaoComponent }
];

export const routing = RouterModule.forRoot(routes);

