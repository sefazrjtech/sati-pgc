import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetodgestaoComponent } from './metodgestao.component';

describe('MetodgestaoComponent', () => {
  let component: MetodgestaoComponent;
  let fixture: ComponentFixture<MetodgestaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetodgestaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetodgestaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
