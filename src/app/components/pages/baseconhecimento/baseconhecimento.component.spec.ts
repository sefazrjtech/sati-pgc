import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseconhecimentoComponent } from './baseconhecimento.component';

describe('BaseconhecimentoComponent', () => {
  let component: BaseconhecimentoComponent;
  let fixture: ComponentFixture<BaseconhecimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseconhecimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseconhecimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
