import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SgpdComponent } from './sgpd.component';

describe('SgpdComponent', () => {
  let component: SgpdComponent;
  let fixture: ComponentFixture<SgpdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SgpdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SgpdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
