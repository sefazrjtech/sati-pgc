import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OquemudaComponent } from './oquemuda.component';

describe('OquemudaComponent', () => {
  let component: OquemudaComponent;
  let fixture: ComponentFixture<OquemudaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OquemudaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OquemudaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
