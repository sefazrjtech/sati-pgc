import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { MenuComponent } from './menu/menu.component';
import { routing } from '../../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  exports: [
    HeaderComponent,
  ],
  declarations: [
    HeaderComponent,
    MenuComponent
  ]
})
export class HeaderModule { }
